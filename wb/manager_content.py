"""Content Manager."""
# import resources
import util


class ContentManager():
    """Content Manager."""

    def scroll_to_outlook(self):
        """Scroll to the Outloook card."""
        try:
            # element = util.get_object_with_text(driver, resources.CONTENT_CARDS_OUTLOOK_LBL)
            # driver.execute_script('mobile: scroll', {"element": element, "toVisible": True})
            # util.select_object_by_resource_id(resources.CONTENT_CARDS_OUTLOOK_LBL)
            util.swipe_up()
            # swipeup
        except Exception, e:
            util.log_error("Couldn't scroll to Outlook Card: " + str(e))
