"""Resource Constants."""

# WELCOME
WELCOME_BUTTON_01_RES_ID = "com.aws.android:id/button_welcome_activity_next"

# Home Tab
HOME_TAB_NOW_RES_ID = "com.aws.android:id/tabNow"
HOME_TAB_HOURLY_RES_ID = "com.aws.android:id/tabHourly"
HOME_TAB_TEN_DAY_RES_ID = "com.aws.android:id/tabTenDays"
HOME_TAB_MAPS_RES_ID = "com.aws.android:id/tabMaps"

# Now Tab
NOW_TAB_CONTANIER_RES_ID = "com.aws.android:id/nowMainContainer"
NOW_TAB_SCROLL_RES_ID = "com.aws.android:id/content"
NOW_TAB_WEATHER_STATION_RES_ID = "com.aws.android:id/textView_now_fragment_weather_station"
NOW_TAB_LAST_UPDATE_TIME_RES_ID = "com.aws.android:id/last_updated"
NOW_TAB_CURRENT_TEMPERATURE_CONTAINER_RES_ID = "com.aws.android:id/relativeLayout_now_fragment_temperature_container"
NOW_TAB_CURRENT_TEMP_RES_ID = "com.aws.android:id/textView_now_fragment_current_temperature"
NOW_TAB_CURRENT_TEMP_UNITS_RES_ID = "com.aws.android:id/textView_now_fragment_current_temperature_unit"
NOW_TAB_FEELS_LIKE_TEMP_RES_ID = "com.aws.android:id/textView_now_fragment_feels_like_value"
NOW_TAB_HI_TEMP_RES_ID = "com.aws.android:id/textView_now_fragment_high_temperature_value"
NOW_TAB_LO_TEMP_RES_ID = "com.aws.android:id/textView_now_fragment_low_temperature_value"
NOW_TAB_CURRENT_CONDITIONS_RES_ID = "com.aws.android:id/relativeLayout_now_fragment_current_conditions"
NOW_TAB_CURRENT_CONDITONS_ICON_RES_ID = "com.aws.android:id/imageView_now_fragment_current_conditions"
NOW_TAB_CURRENT_CONDITIONS_DESC_RES_ID = "com.aws.android:id/textView_now_fragment_current_conditions"
NOW_TAB_SPOTLIGHT_ORDER_BUTTON_RES_ID = "com.aws.android:id/button_now_fragment_configure_spotlight"

# Content Cards
CONTENT_CARDS_OUTLOOK_LBL = "Outlook"

# Side Menu
SIDE_MENU_BUTTON_RES_ID = "com.aws.android:id/action_bar_img_app_icon"
SIDE_MENU_OPTION_RES_ID = "com.aws.android:id/navigation_drawer_item_text"
SIDE_MENU_OPTION_SIGN_IN_LBL = "Sign In"
SIDE_MENU_OPTION_SETTINGS_LBL = "Settings"
SIDE_MENU_OPTION_ALERTS_LBL = "Alert Notifications"
SIDE_MENU_OPTION_FAQ_LBL = "FAQ"
SIDE_MENU_OPTION_ABOUT_LBL = "About"
SIDE_MENU_OPTION_TERMS_LBL = "Terms"
SIDE_MENU_OPTION_FEEDBACK_LBL = "Send Feedback"
SIDE_MENU_OPTION_SHARE_LBL = "Share WeatherBug"
SIDE_MENU_OPTION_MAPS_LBL = "Maps"
SIDE_MENU_OPTION_SPARK_LBL = "Lightning"

# Location Manager
LOCATION_SPINNER_RES_ID = "com.aws.android:id/location_name"
LOCATION_BACK_ARROW_CLASS = "android.widget.ImageButton"
LOCATION_TEXT_FIELD = "com.aws.android:id/search_edit_text"
LOCATION_ADD_BUTTON_RES_ID = "com.aws.android:id/add_location_fab"
LOCATION_DELETE_RES_ID = "com.aws.android:id/delete_fab"
LOCATION_RESULTS_RES_ID = "com.aws.android:id/search_matches_recycler_view"
LOCATION_SAVE_BUTTON_RES_ID = "com.aws.android:id/save_image_button"
SAVED_LOCATION_CARDS = "com.aws.android:id/locations_recycler_view"
SWIPE_DELETE_BUTTON_RES_ID = "com.aws.android:id/yes_button"
SWIPE_CANCEL_BUTTON_RES_ID = "com.aws.android:id/no_button"
LOCATION_EDIT_LOCATION_NAME_RES_ID = "com.aws.android:id/location_name_edit_text"
LOCATION_MANAGER_LOCATION_CONTAINER = "com.aws.android:id/container"
LOCATION_ENVIRONMENT_SERVER_RES_ID = "com.aws.android:id/spinner_server"
LOCATION_ENVIRONMENT_PORT_RES_ID = "com.aws.android:id/spinner_port"
LOCATION_ENVIRONMENT_SAVE_BUTTON_RES_ID = "com.aws.android:id/button_save"

# Sign In Page
SIGN_IN_PAGE_CLOSE_BUTTON_RES_ID = "com.aws.android:id/action_bar_img_close"

# Settings Page
SETTINGS_PAGE_CLOSE_BUTTON_CLASS = "android.widget.ImageButton"

# Notifications Page
NOTIFICATIONS_PAGE_CLOSE_BUTTON_CLASS = "android.widget.ImageButton"

# FAQ Page
FAQ_PAGE_CLOSE_BUTTON_CLASS = "android.widget.ImageButton"

# About Page

# Terms And Conditions Page
TNC_PAGE_CLOSE_BUTTON_RES_ID = "com.aws.android:id/decline"

# Send Feedback

# Share WeatherBug

# Spark Page
SPARK_PAGE_CLOSE_BUTTON_CLASS = "android.widget.ImageButton"
