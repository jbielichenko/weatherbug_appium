"""Notifications Page."""
import resources
import util


class NotificationsPage():
    """Notifications Page."""

    def close(self):
        """Close Notifications Page."""
        try:
            util.select_object_by_class(resources.NOTIFICATIONS_PAGE_CLOSE_BUTTON_CLASS)
        except Exception, e:
            util.log_error("Couldn't close Notifications page: " + str(e))
