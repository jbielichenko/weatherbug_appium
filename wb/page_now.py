"""Now Tab."""
import resources
import util


class NowPage():
    """Now Tab."""

    def get_current_temp(self):
        """Return the current temperature on Now."""
        return self.__get_value(resources.NOW_TAB_CURRENT_TEMP_RES_ID, "Couldn't get current temp on Now: ")

    def get_feels_like(self):
        """Return the feels like temperature on Now."""
        return self.__get_value(resources.NOW_TAB_FEELS_LIKE_TEMP_RES_ID, "Couldn't get Feels Like temp on Now: ")

    def get_hi(self):
        """Return the Hi temperature on Now."""
        return self.__get_value(resources.NOW_TAB_HI_TEMP_RES_ID, "Couldn't get Hi temp on Now: ")

    def get_lo(self):
        """Return the Lo temperature on Now."""
        return self.__get_value(resources.NOW_TAB_LO_TEMP_RES_ID, "Couldn't get Lo temp on Now: ")

    def __get_value(self, resource, error_message):
        try:
            return util.get_text(resource)
        except Exception, e:
            util.log_error(error_message + str(e))
