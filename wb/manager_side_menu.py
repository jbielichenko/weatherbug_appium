"""Side Menu Manager."""
import resources
import util
from time import sleep


class SideMenuManager():
    """Side Menu Manager."""

    def open_side_menu(self, wait=1):
        """Open Side Menu."""
        try:
            util.select_object_by_resource_id(resources.SIDE_MENU_BUTTON_RES_ID)
            sleep(wait)
        except Exception, e:
            util.log_error("Couldn't open side menu: " + str(e))

    def close_side_menu(self):
        """Close Side Menu."""
        try:
            util.select_object_by_resource_id(resources.SIDE_MENU_BUTTON_RES_ID)
        except Exception, e:
            util.log_error("Couldn't close side menu: " + str(e))

    def select_sign_in_menu(self):
        """Select Sign In Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_SIGN_IN_LBL, "Couldn't select Sign In menu option: ", 10)

    def select_settings_menu(self):
        """Select Settings Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_SETTINGS_LBL, "Couldn't select Settings menu option: ")

    def select_alerts_menu(self):
        """Select Alert Notifications Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_ALERTS_LBL, "Couldn't select Settings menu option: ")

    def select_faq_menu(self):
        """Select FAQQ Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_FAQ_LBL, "Couldn't select FAQ menu option: ")

    def select_about_menu(self):
        """Select About Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_FAQ_LBL, "Couldn't select About menu option: ")

    def select_terms_menu(self):
        """Select Terms Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_TERMS_LBL, "Couldn't select Terms menu option: ")

    def select_feedback_menu(self):
        """Select Send Feedback Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_FEEDBACK_LBL, "Couldn't select Send Feedback menu option: ")

    def select_share_menu(self):
        """Select Share WeatherBug Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_SHARE_LBL, "Couldn't select Share WeatherBug menu option: ")

    def select_maps_menu(self):
        """Select Maps Menu Option."""
        self.__select_menu_option(resources.SIDE_MENU_OPTION_MAPS_LBL, "Couldn't select Maps menu option: ")

    def select_lightning_menu(self):
        """Select Lightning Menu Option."""
        try:
            self.open_side_menu(3)
            util.select_object_containing_text(resources.SIDE_MENU_OPTION_SPARK_LBL, 5)
        except Exception, e:
            util.log_error("Couldn't select Lightning menu option: " + str(e))

    def __select_menu_option(self, resource, error_message, wait=0):
        try:
            self.open_side_menu()
            util.select_object_with_text(resource, wait)
        except Exception, e:
            util.log_error(error_message + str(e))
