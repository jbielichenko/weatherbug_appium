"""Home Tab Manager."""
import res_ids
import util


class HomeTabManager():
    """Home Tab Manager."""

    def select_now_tab(self):
        """Select the Now Tab."""
        self.__select_tab(res_ids.HOME_TAB_NOW_RES_ID, "Couldn't select Now tab: ")

    def select_hourly_tab(self):
        """Select the Hourly Tab."""
        self.__select_tab(res_ids.HOME_TAB_HOURLY_RES_ID, "Couldn't select Hourly tab: ")

    def select_ten_day_tab(self):
        """Select the 10 Day Ta."""
        self.__select_tab(res_ids.HOME_TAB_TEN_DAY_RES_ID, "Couldn't select 10 Day tab: ")

    def select_maps_tab(self):
        """Select the Maps Tab."""
        self.__select_tab(res_ids.HOME_TAB_MAPS_RES_ID, "Couldn't select Maps tab: ")

    def __select_tab(self, resource, error_message):
        try:
            util.select_object_by_resource_id(resource, 3)
        except Exception, e:
            util.log_error(error_message + str(e))
