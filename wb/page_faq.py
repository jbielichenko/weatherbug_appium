"""FAQ Page."""
import resources
import util


class FaqPage():
    """FAQ Page."""

    def close(self):
        """Close FAQ Page."""
        try:
            util.select_object_by_class(resources.FAQ_PAGE_CLOSE_BUTTON_CLASS)
        except Exception, e:
            util.log_error("Couldn't close FAQ page: " + str(e))
