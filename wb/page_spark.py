"""Spark Page."""
import resources
import util


class SparkPage():
    """Spark Page."""

    def close(self):
        """Close Spark Page."""
        try:
            util.select_object_by_class(resources.SPARK_PAGE_CLOSE_BUTTON_CLASS)
        except Exception, e:
            util.log_error("Couldn't close Spark page: " + str(e))
