"""Location Manager."""
import resources
import util
from time import sleep


class LocationManager():
    """Location Manager."""

    def open_location_manager(self, wait=1):
        """Open Location Manager."""
        try:
            util.select_object_by_resource_id(resources.LOCATION_SPINNER_RES_ID)
            sleep(wait)
        except Exception, e:
            util.log_error("Couldn't open Location Manager: " + str(e))

    def close_location_manager(self):
        """Close Location Manager."""
        try:
            util.select_object_by_class(resources.LOCATION_BACK_ARROW_CLASS)
        except Exception, e:
            util.log_error("Couldn't close Location Manager: " + str(e))

    def add_locations(self, locations):
        """Add multiple locations."""
        for location in locations:
            self.add_location(location)

    def add_location(self, location):
        """Add a single location."""
        self.open_location_manager()
        self.__select_add_button()
        self.__enter_location(location)
        self.__select_location(location)
        self.__save_location()
        self.close_location_manager()

    def change_location(self, location):
        """Change the viewed location."""
        self.open_location_manager()
        self.__select_saved_location(location)

    def set_environment(self, environment=5, port=9000):
        """Set the environment to 0=Prod, 1=Stg, 2=QA, 3=Dev, 4=Prod-Node.js, 5=QA-Node.js."""
        self.open_location_manager()
        self.__select_add_button()
        self.__enter_location("*123")
        self.__select_environment(environment, port)
        self.close_location_manager()

    def __select_add_button(self):
        try:
            util.select_object_by_resource_id(resources.LOCATION_ADD_BUTTON_RES_ID)
        except Exception, e:
            util.log_error("Couldn't select add button: " + str(e))

    def __enter_location(self, location):
        try:
            util.set_text(resources.LOCATION_TEXT_FIELD, location)
        except Exception, e:
            util.log_error("Couldn't enter location: " + str(e))

    def __select_location(self, location):
        try:
            util.select_child_index_for_object_by_resource_id(resources.LOCATION_RESULTS_RES_ID)
        except Exception, e:
            util.log_error("Couldn't select location from search results: " + str(e))

    def __save_location(self):
        try:
            util.select_object_by_resource_id(resources.LOCATION_SAVE_BUTTON_RES_ID)
        except Exception, e:
            util.log_error("Couldn't save location: " + str(e))

    def __select_saved_location(self, location):
        try:
            util.select_object_containing_text(location)
            # TODO: Add scrolling to search for location
        except Exception, e:
            util.log_error("Couldn't select the saved location: " + str(e))

    def __select_environment(self, environment, port):
        environments = {0: "Prod",
                        1: "Stg",
                        2: "QA",
                        3: "Dev",
                        4: "Cloud",
                        5: "Mountain"}
        try:
            util.select_object_by_resource_id(resources.LOCATION_ENVIRONMENT_SERVER_RES_ID)
            util.select_object_containing_text(environments[environment])
            if(environment == 4 | environment == 5):
                    self.__select_port(port)
            util.select_object_by_resource_id(resources.LOCATION_ENVIRONMENT_SAVE_BUTTON_RES_ID)
        except Exception, e:
            util.log_error("Couldn't change environment: " + str(e))

    def __select_port(self, port):
        try:
            util.select_object_by_resource_id(resources.LOCATION_ENVIRONMENT_PORT_RES_ID)
            util.select_object_containing_text(str(port))
        except Exception, e:
            util.log_error("Couldn't change environment: " + str(e))
