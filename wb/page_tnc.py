"""Terms And Conditions Page."""
import resources
import util


class TermsAndConditionsPage():
    """Terms And Conditions Page."""

    def close(self):
        """Close Terms And Conditions Page."""
        try:
            util.select_object_by_resource_id(resources.TNC_PAGE_CLOSE_BUTTON_RES_ID)
        except Exception, e:
            util.log_error("Couldn't close Terms And Conditions page: " + str(e))
