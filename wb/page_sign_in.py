"""Sign In Page."""
import resources
import util


class SignInPage():
    """Side Menu Manager."""

    def close(self):
        """Close Sign In Page."""
        try:
            util.select_object_by_resource_id(resources.SIGN_IN_PAGE_CLOSE_BUTTON_RES_ID)
        except Exception, e:
            util.log_error("Couldn't close Sign In page: " + str(e))
