"""Settings Page."""
import resources
import util


class SettingsPage():
    """Settings Page."""

    def close(self):
        """Close Settings Page."""
        try:
            util.select_object_by_class(resources.SETTINGS_PAGE_CLOSE_BUTTON_CLASS)
        except Exception, e:
            util.log_error("Couldn't close Settings page: " + str(e))
