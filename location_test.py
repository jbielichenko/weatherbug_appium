"""Test the Location Manager."""
import unittest
import sys
import util
from appium import webdriver
from time import sleep
from wb.manager_location import LocationManager


class LocationTest(unittest.TestCase):
    """LocationTest."""

    def setUp(self):
        """Set the capabilities and the driver."""
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'  # '6.1.0'
        desired_caps['deviceName'] = 'Samsung SM-N910V'  # 'LGE Nexus 5X'
        desired_caps['appPackage'] = 'com.aws.android'
        desired_caps['appActivity'] = 'app.ui.HomeActivity'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        """Clean up."""
        sleep(10)
        self.driver.quit()

    def bypass_welcome(self):
        """Bypass the Welcome screen."""
        sleep(1)
        res_ids = util.load_values("./wb/resources.py")
        resource_id = res_ids["WELCOME_BUTTON_01_RES_ID"]
        util.select_object_by_resource_id(self.driver, resource_id)
        sleep(3)

    def test_add_locations(self):
        """Adding locations."""
        self.bypass_welcome()

        test = LocationManager()
        test.add_location(self.driver, "Fairfield, California")

        locations = util.load_list("./data/data_driver_us_locations.txt")
        test.add_locations(self.driver, locations)

        test.change_location(self.driver, "Los Angeles, California")

        sleep(15)


# ---START OF TEST
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(LocationTest)
    ret = not unittest.TextTestRunner(verbosity=2).run(suite).wasSuccessful()
    sys.exit(ret)
