"""Test the Home Tabs by selecting each Tab (Hourly, 10 Day, Maps, and Now."""
import unittest
import sys
import util
from appium import webdriver
from wb.manager_home_tab import HomeTabManager
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
# from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0


class HomeTabTest(unittest.TestCase):
    """HomeTabTest."""

    def setUp(self):
        """Set the capabilities and the driver."""
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'  # '6.1.0'
        desired_caps['deviceName'] = 'Samsung SM-N910V'  # 'LGE Nexus 5X'
        desired_caps['appPackage'] = 'com.aws.android'
        desired_caps['appActivity'] = 'app.ui.HomeActivity'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        util.init(self.driver)

    def tearDown(self):
        """Clean up."""
        self.driver.quit()

    def test_tab_selection(self):
        """Select home tabs."""
        util.bypass_welcome()

        test = HomeTabManager()
        test.select_maps_tab()
        test.select_ten_day_tab()
        test.select_hourly_tab()
        test.select_now_tab()


# ---START OF TEST
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(HomeTabTest)
    # suite = unittest.TestLoader().loadTestsFromTestCase(testOnboarding)
    ret = not unittest.TextTestRunner(verbosity=2).run(suite).wasSuccessful()
    sys.exit(ret)
