"""Test the Side Menu by selecting each menu option."""
import unittest
import sys
import util
from appium import webdriver
from time import sleep
from wb.manager_side_menu import SideMenuManager
from wb.page_sign_in import SignInPage
from wb.page_settings import SettingsPage
from wb.page_notifications import NotificationsPage
from wb.page_faq import FaqPage
from wb.page_tnc import TermsAndConditionsPage
from wb.page_spark import SparkPage


class SideMenuTest(unittest.TestCase):
    """SideMenuTest."""

    def setUp(self):
        """Set the capabilities and the driver."""
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'  # '6.1.0'
        desired_caps['deviceName'] = 'Samsung SM-N910V'  # 'LGE Nexus 5X'
        desired_caps['appPackage'] = 'com.aws.android'
        desired_caps['appActivity'] = 'app.ui.HomeActivity'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        util.init(self.driver)

    def tearDown(self):
        """Clean up."""
        sleep(10)
        self.driver.quit()

    def test_tab_selection(self):
        """Select menu options."""
        util.bypass_welcome()

        test = SideMenuManager()

        test.select_sign_in_menu()
        sleep(2)
        sign_in_page = SignInPage()
        sign_in_page.close()

        test.select_settings_menu()
        sleep(2)
        settings_page = SettingsPage()
        settings_page.close()

        test.select_alerts_menu()
        sleep(2)
        notifications_page = NotificationsPage()
        notifications_page.close()

        test.select_faq_menu()
        sleep(2)
        faq_page = FaqPage()
        faq_page.close()

        # TODO: About

        test.select_terms_menu()
        sleep(2)
        tnc_page = TermsAndConditionsPage()
        tnc_page.close()

        # TODO: Send Feedback

        # TODO: Share WeatherBug

        # TODO: Maps

        test.select_lightning_menu()
        sleep(2)
        spark_page = SparkPage()
        spark_page.close()


# ---START OF TEST
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SideMenuTest)
    ret = not unittest.TextTestRunner(verbosity=2).run(suite).wasSuccessful()
    sys.exit(ret)
