"""Test the Content Cards on the Now Page."""
import unittest
import sys
import util
from appium import webdriver
from time import sleep
from wb.manager_content import ContentManager


class ContentCardTest(unittest.TestCase):
    """NowPageTest."""

    def setUp(self):
        """Set the capabilities and the driver."""
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'  # '6.1.0'
        desired_caps['deviceName'] = 'Samsung SM-N910V'  # 'LGE Nexus 5X'
        desired_caps['appPackage'] = 'com.aws.android'
        desired_caps['appActivity'] = 'app.ui.HomeActivity'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        util.init(self.driver)

    def tearDown(self):
        """Clean up."""
        sleep(10)
        self.driver.quit()

    def test_content_cards(self):
        """Test the Content Cards."""
        self.bypass_welcome()
        sleep(1)
        content = ContentManager()
        content.scroll_to_outlook()

        sleep(15)


# ---START OF TEST
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(ContentCardTest)
    ret = not unittest.TextTestRunner(verbosity=2).run(suite).wasSuccessful()
    sys.exit(ret)
