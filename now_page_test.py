"""Test the Now Page."""
import unittest
import sys
import util
from appium import webdriver
from time import sleep
from wb.manager_location import LocationManager
from wb.page_now import NowPage


class NowPageTest(unittest.TestCase):
    """NowPageTest."""

    def setUp(self):
        """Set the capabilities and the driver."""
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'  # '6.1.0'
        desired_caps['deviceName'] = 'Samsung SM-N910V'  # 'LGE Nexus 5X'
        desired_caps['appPackage'] = 'com.aws.android'
        desired_caps['appActivity'] = 'app.ui.HomeActivity'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        util.init(self.driver)

    def tearDown(self):
        """Clean up."""
        sleep(10)
        self.driver.quit()

    def test_verify_now_page_data(self):
        """Verify Now data from node server."""
        util.bypass_welcome()
        sleep(1)
        loc_mgr = LocationManager()
        loc_mgr.set_environment()

        # loc_mgr.add_location("New York, New York")
        loc_mgr.add_location("San Jose, California")

        test = NowPage()
        expected_value = "79"
        current_value = test.get_current_temp()
        print("Current Temp: " + current_value)
        util.log_result(self, expected_value, current_value, "1234267")

        expected_value = "78"
        current_value = test.get_feels_like()
        print("Current Feels Like: " + current_value)
        util.log_result(self, expected_value, current_value, "1234267")

        expected_value = "80"
        current_value = test.get_hi()
        print("Current Hi: " + current_value)
        util.log_result(self, expected_value, current_value, "1234267")

        expected_value = "60"
        current_value = test.get_lo()
        print("Current Lo: " + current_value)
        util.log_result(self, expected_value, current_value, "1234267")

        sleep(15)


# ---START OF TEST
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(NowPageTest)
    ret = not unittest.TextTestRunner(verbosity=2).run(suite).wasSuccessful()
    sys.exit(ret)
