"""Utility file for common functionality."""

from time import sleep
from logging import Logger


def init(driver):
    """Set the driver, screen_size and log."""
    global wb_driver, screen_size, log

    try:
        wb_driver
    except NameError:
        wb_driver = driver
    settings = wb_driver.get_settings()
    print("Settings: " + str(settings))

    try:
        screen_size
    except NameError:
        screen_size = get_driver().get_window_size()
    print screen_size

    try:
        log
    except NameError:
        log = Logger("Automation Logger", level="DEBUG")


def get_driver():
    """Rerturn the driver."""
    return wb_driver


def get_screen_size():
    """Return size of screen for device."""
    return screen_size


def load_values(file_name):
    """Load key value pairs from a file."""
    key_values = {}
    with open(file_name, 'r') as key_value_file:
        for line in key_value_file:
            if("=" in line):
                key, value = line.partition("=")[::2]
                # print(key + " : " + value)
                key_values[key.strip()] = value.strip()
    return key_values


def load_list(file_name):
    """Load list of values from a file."""
    values = []
    with open(file_name, 'r') as list_file:
        for line in list_file:
            value = line.strip()
            # print ("Value: " + value)
            if(len(value) > 0):
                values.append(value)
    return values


def log_error(message):
    """Log an error."""
    message = "Exception - " + message
    print(message)
    log.error(message)


def log_result(self, expected_value, current_value, test_case_id):
    """Log a test case result."""
    print "Expected Value: " + expected_value
    print "Current Value: " + current_value
    try:
        self.assertEqual(expected_value, current_value)
        print("Test Case: " + test_case_id + " - PASS")
    except AssertionError, e:
        print("Assertion Error: " + str(e))
        print("Test Case: " + test_case_id + " - FAIL")
    # TODO: Update QAComplete


def select_back_button():
    """Select back button on Android device."""
    # driver.p pressKeyCode(AndroidKeyCode.BACK)
    get_driver.navigate().back()


def verify_label(self, resource_id, expected_value):
    """Assert whether current value is equal to the passed expected value argument."""
    # print(resourceId+":"+expectedValue)
    current_value = get_driver.find_element_by_android_uiautomator('new UiSelector().resourceId(' + resource_id + ')').text
    # print("Current Value: " + currentValue)
    self.assertEqual(current_value, expected_value)


def get_object_by_resource_id(resource_id):
    """Get the object by resource ID."""
    # print("Returning object with resource ID: " + resource_id)
    uia_string = "new UiSelector().resourceId(" + format_string(resource_id) + ")"
    error_message = "Exception - Selecting object by Resource ID: "
    return get_android_object(uia_string, error_message)


def select_object_by_resource_id(resource_id, wait=1):
    """Select the object by resource ID."""
    # print("Selecting object with resource ID: " + resource_id)
    uia_string = "new UiSelector().resourceId(" + format_string(resource_id) + ")"
    error_message = "Exception - Selecting object by Resource ID: "
    select_android_object(uia_string, error_message, wait)


def select_object_with_text(text, wait=1):
    """Select the object with text."""
    # print("Selecting object with text: " + text)
    uia_string = "new UiSelector().text(" + format_string(text) + ")"
    error_message = "Exception - Selecting object with text: "
    select_android_object(uia_string, error_message, wait)


def select_object_containing_text(text, wait=1):
    """Select the object that contains text."""
    # print("Selecting object containing text: " + text)
    uia_string = "new UiSelector().textContains(" + format_string(text) + ")"
    error_message = "Exception - Selecting object containing text: "
    select_android_object(uia_string, error_message, wait)


def select_object_by_class(class_name, wait=1):
    """Select the object with text."""
    # print("Selecting object by class name: " + class_name)
    uia_string = "new UiSelector().className(" + format_string(class_name) + ")"
    error_message = "Exception - Selecting object by class: "
    select_android_object(uia_string, error_message, wait)


def select_child_index_for_object_by_resource_id(resource_id, index=0):
    """Select the child at index for the object by resource ID."""
    uia_string = "new UiSelector().index(" + str(index) + ")"
    # print("UIA String: " + uia_string)
    try:
        element = get_object_by_resource_id(resource_id)
        child = element.find_element_by_android_uiautomator(uia_string)
        child.click()
    except Exception, e:
        print("Exception - Selecting object by Resource ID: " + str(e))


def format_string(text):
    """Put quotes around the string for proper object searching."""
    if not (text.startswith('\"')):
        text = '\"' + text + '\"'
    return text


def get_text(resource_id):
    """Get the text for object by resource ID."""
    # print("Getting text: for object with resource ID: " + resource_id)
    uia_string = "new UiSelector().resourceId(" + format_string(resource_id) + ")"
    # print("UIA String: " + uia_string)
    try:
        element = get_driver().find_element_by_android_uiautomator(uia_string)
        value = element.get_attribute("text")
        # print ("Value: " + value)
        return value
    except Exception, e:
        print("Exception - Setting text for object by Resource ID: " + str(e))


def set_text(resource_id, text, wait=1):
    """Set the text for object by resource ID."""
    # print("Setting text: " + text + " for object with resource ID: " + resource_id)
    uia_string = "new UiSelector().resourceId(" + format_string(resource_id) + ")"
    # print("UIA String: " + uia_string)
    try:
        element = get_driver().find_element_by_android_uiautomator(uia_string)
        # element.set_text(text)
        # element.set_value(text)
        get_driver().set_value(element, text)
        sleep(wait)
    except Exception, e:
        print("Exception - Setting text for object by Resource ID: " + str(e))


def select_android_object(uia_string, error_message, wait=0):
        """Select Android object."""
        # print("UIA String: " + uia_string)
        # print("Waiting " + str(wait) + " sec.")
        try:
            element = get_driver().find_element_by_android_uiautomator(uia_string)
            element.click()
            sleep(wait)
        except Exception, e:
            print(error_message + str(e))


def get_android_object(uia_string, error_message):
        """Get Android object."""
        # print("UIA String: " + uia_string)
        try:
            element = get_driver().find_element_by_android_uiautomator(uia_string)
            return element
        except Exception, e:
            print(error_message + str(e))


def swipe_up(duration=None):
    """Swipe up on screen."""
    width = get_screen_size()["width"]
    height = get_screen_size()["length"]
    center = width / 2
    print("Center: " + str(center))
    print("Width: " + str(width))
    print("Height: " + str(height))
    try:
        get_driver().swipe(center, height - 1, center, 1, duration)
    except Exception, e:
        print("Exception - Could not swipe up: " + str(e))


def bypass_welcome():
    """Bypass the Welcome screen."""
    try:
        select_object_by_resource_id("com.aws.android:id/button_welcome_activity_next", 5)
    except Exception, e:
        print("Exception - Bypassing Welcome: " + str(e))
